package pitzik4.games.historygame;

import flash.events.Event;
import flash.display.StageDisplayState;

import flixel.FlxState;
import flixel.FlxG;

class SetupState extends FlxState {
  public var state:FlxState;
  
  override public function create():Void {
    cast(FlxG.game, HistoryGame).stage_onResize();
    FlxG.stage.addEventListener(Event.RESIZE, cast(FlxG.game, HistoryGame).stage_onResize);
    Settings.load();
    #if desktop
    if(Settings.fullscreen) {
      FlxG.stage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE;
    }
    #end
  }
  override public function update():Void {
    if(state == null) {
      state = new TitleState();
    }
    FlxG.switchState(state);
  }
}
