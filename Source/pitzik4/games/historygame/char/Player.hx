package pitzik4.games.historygame.char;

import flixel.FlxSprite;
import flixel.FlxG;
import flixel.FlxObject;

class Player extends FlxSprite {
  inline public static var MOVE_SPEED:Float = 64;
  
  public function new(x:Float, y:Float) {
    super(x, y);
    loadGraphic("assets/gfx/char.png", true, true, 32, 32);
    animation.add("runs", [ 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12], 15);
    animation.add("rund", [17,18,19,20,21,22,23,24,25,26,27,28], 15);
    animation.add("runu", [33,34,35,36,37,38,39,40,41,42,43,44], 15);
    animation.add("idles", [ 0]);
    animation.add("idled", [16]);
    animation.add("idleu", [32]);
    animation.add("slides", [13, 0], 3, false);
    animation.add("slided", [29,16], 3, false);
    animation.add("slideu", [45,32], 3, false);
    animation.play("idles");
  }
  override public function update():Void {
    super.update();
    drag.x = drag.y = 0;
    var going:Bool = false;
    if(FlxG.keys.pressed.W) {
      animation.play("runu");
      facing = FlxObject.UP;
      velocity.y = -MOVE_SPEED;
      going = true;
    } else if(FlxG.keys.pressed.S) {
      animation.play("rund");
      facing = FlxObject.DOWN;
      velocity.y = MOVE_SPEED;
      going = true;
    } else {
      drag.y = MOVE_SPEED*4;
    }
    if(FlxG.keys.pressed.A) {
      animation.play("runs");
      facing = FlxObject.LEFT;
      velocity.x = -MOVE_SPEED;
      going = true;
    } else if(FlxG.keys.pressed.D) {
      animation.play("runs");
      facing = FlxObject.RIGHT;
      velocity.x = MOVE_SPEED;
      going = true;
    } else {
      drag.x = MOVE_SPEED*4;
    }
    if(!going && (velocity.y != 0 || velocity.x != 0)) {
      if(facing == FlxObject.UP) {
        animation.play("slideu");
      } else if(facing == FlxObject.DOWN) {
        animation.play("slided");
      } else {
        animation.play("slides");
      }
    }
  }
}
