package pitzik4.games.historygame;

#if flash
import flash.net.SharedObject;
#elseif !html5
import sys.io.File;
import sys.FileSystem;
import haxe.Json;
#end

class Settings {
  inline public static var CFG_FILE_NAME:String = "hgsettings.json";
  inline public static var CFG_SO_NAME:String = "hgsettings";
  public static var fullscreen:Bool = false;
  
  public static function load():Void {
    #if flash
    var so:SharedObject = SharedObject.getLocal(CFG_SO_NAME);
    //if(so.data.fullscreen != null)
    //  fullscreen = cast so.data.fullscreen;
    #elseif !html5
    if(FileSystem.exists(CFG_FILE_NAME)) {
      var settings:Dynamic = Json.parse(File.getContent(CFG_FILE_NAME));
      if(settings.fullscreen != null)
        fullscreen = cast settings.fullscreen;
    }
    #end
  }
  public static function save():Void {
    #if flash
    var so:SharedObject = SharedObject.getLocal(CFG_SO_NAME);
    //so.setProperty("fullscreen", fullscreen);
    #elseif !html5
    File.saveContent(CFG_FILE_NAME, Json.stringify({fullscreen:fullscreen}));
    #end
  }
}
