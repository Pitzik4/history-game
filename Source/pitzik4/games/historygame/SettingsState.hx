package pitzik4.games.historygame;

import flash.display.StageDisplayState;

import flixel.FlxState;
import flixel.FlxG;
import flixel.group.FlxSpriteGroup;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import pitzik4.games.historygame.ui.UICheckbox;

class SettingsState extends FlxState {
  private var title:FlxText;
  private var cs:FlxSpriteGroup;
  private var fscheck:UICheckbox;
  
  override public function create():Void {
    FlxG.cameras.bgColor = 0xFF000040;
    
    cs = new FlxSpriteGroup(3);
    
    cs.add(title = new FlxText(0, 0, 100, "Settings"));
    title.alignment = "center";
    var ypos:Int = 16;
    
    fscheck = new UICheckbox(10, ypos, "Fullscreen", Settings.fullscreen);
    #if !mobile
    cs.add(fscheck);
    ypos += 24;
    #end
    
    var button:FlxButton = new FlxButton(10, ypos, "Apply", apply);
    cs.add(button);
    ypos += 24;
    button = new FlxButton(10, ypos, "Back", function():Void {
      FlxG.switchState(new TitleState());
    });
    cs.add(button);
    ypos += 24;
    
    add(cs);
    
    add(new CurSprite());
    onResize(FlxG.stage.stageWidth, FlxG.stage.stageHeight);
  }
  override public function onResize(width:Int, height:Int):Void {
    super.onResize(width, height);
    width = cast width/FlxG.camera.zoom;
    height = cast height/FlxG.camera.zoom;
    cs.x = width/2-50;
    cs.y = height/2-40;
  }
  public function apply():Void {
    if(fscheck.checked && !Settings.fullscreen) {
      FlxG.stage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE;
    } else if(!fscheck.checked && Settings.fullscreen) {
      FlxG.stage.displayState = StageDisplayState.NORMAL;
    }
    Settings.fullscreen = fscheck.checked;
    Settings.save();
    FlxG.switchState(new SettingsState());
  }
}
